import React from 'react'
import Calander from './CalendarComponent'
// import AppointmentCalendar from './AppointmentCalendar'

const App = () => {

  const sampleResourceData = [
    { id: 'user1', name: 'User 1' },
    { id: 'user2', name: 'User 2' },
    { id: 'user3', name: 'User 3' },
  ];
  
  const sampleAppointmentData = [
    {
      userId: 'user1',
      title: 'Meeting with Client',
      start: '2023-08-16T09:00:00',
      end: '2023-08-16T10:30:00',
    },
    {
      userId: 'user2',
      title: 'Team Meeting',
      start: '2023-08-16T11:00:00',
      end: '2023-08-16T12:30:00',
    },
    {
      userId: 'user3',
      title: 'Presentation',
      start: '2023-08-16T14:00:00',
      end: '2023-08-16T15:30:00',
    },
  ];

  return (
    <div>
      <h1>Appointment Calendar</h1>
      <Calander
        appointmentData={sampleAppointmentData}
        resourceData={sampleResourceData}
      />
    </div>
  )
}

export default App