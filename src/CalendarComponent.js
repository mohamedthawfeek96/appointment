import React, { useState } from 'react';
import FullCalendar from '@fullcalendar/react';
import dayGridPlugin from '@fullcalendar/daygrid';
import resourceTimelinePlugin from '@fullcalendar/resource-timeline';
import interactionPlugin from '@fullcalendar/interaction';

const AppointmentCalendar = ({ appointmentData, resourceData }) => {
  const [selectedUser, setSelectedUser] = useState(null);
  const [calendarEvents, setCalendarEvents] = useState([]);

  const convertToCalendarEvents = (userId) => {
    const events = [];
    appointmentData.forEach(appointment => {
      if (appointment.userId === userId) {
        events.push({
          title: appointment.title,
          start: appointment.start,
          end: appointment.end,
        });
      }
    });
    return events;
  };

  const handleUserClick = (userId) => {
    setSelectedUser(userId);
    setCalendarEvents(convertToCalendarEvents(userId));
  };

  return (
    <div className="calendar-container">
      <div className="user-list">
   
      </div>
      <div className="calendar">
        <FullCalendar
          plugins={[dayGridPlugin, resourceTimelinePlugin, interactionPlugin]}
          initialView="resourceTimelineWeek" // Use resourceTimelineWeek for the timeline view
          resources={resourceData} // Provide the array of resource objects
          events={calendarEvents}
          eventColor="blue"
          headerToolbar={{
            left: 'prev,next today',
            center: 'title',
            right: 'resourceTimelineWeek,dayGridMonth,timeGridWeek,timeGridDay',
          }}
          contentHeight="auto"
        />
      </div>
    </div>
  );
};

export default AppointmentCalendar;
